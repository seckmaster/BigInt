import java.util.Iterator;

class CollectionException extends Exception {
	public CollectionException(String msg) {
         super(msg);
    }
}

interface Collection {
    static final String ERR_MSG_EMPTY = "Collection is empty.";
    static final String ERR_MSG_FULL = "Collection is full.";
    static final String ERR_MSG_NULL = "Argument is null.";

    boolean isEmpty();
    boolean isFull();
    int count();
    String toString();
}

interface IStack<T> extends Collection {
    T top() throws CollectionException;
    void push(T x) throws CollectionException;
    T pop() throws CollectionException;
    Iterator<T> getIterator();
}

class Stack<T> implements IStack<T> {
	
	public Stack() {
		data = (T[]) new Object[size];
	}

	public Stack(Stack o) {
		data = (T[]) new Object[size];
		for (Iterator<T> i = o.getIterator(); i.hasNext();)
			try {
				push(i.next());
			} catch (CollectionException e) {
				e.printStackTrace();
			}
	}

	@Override
	public T pop() throws CollectionException {
		if (isEmpty()) throw new CollectionException(ERR_MSG_EMPTY);
		return data[--count];
	}
	
	@Override
	public T top() throws CollectionException {
		if (isEmpty()) throw new CollectionException(ERR_MSG_EMPTY);
		return data[count - 1];
	}
	
	@Override
	public void push(T element) throws CollectionException {
		if (isFull()) throw new CollectionException(ERR_MSG_FULL);
		if (element == null) throw new CollectionException(ERR_MSG_NULL);
		data[count++] = element;
	}

	@Override
	public Iterator<T> getIterator() {
		return new Iterator<T>() {
			
			@Override
			public T next() {
				return data[index++];
			}
			
			@Override
			public boolean hasNext() {
				return (index < count);
			}
			
			@Override
			public void remove() {
			    throw new UnsupportedOperationException();
			}
			
			private int index = 0;
		};
	}
	
	@Override
	public boolean isEmpty() {
		return count == 0;
	}
	
	@Override
	public boolean isFull() {
		return count == size;
	}
	
	@Override
	public int count() {
		return count;
	}
	
	public void clear() {
		count = 0;
		data = (T[]) new Object[size];
	}
	
	private T[] data;
	private int size  = 64;
	private int count = 0;
}
