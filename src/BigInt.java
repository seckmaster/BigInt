
class BigInt implements Comparable<BigInt> {
	
	/**
	 * Constructors.
	 */
	public BigInt() {
		digits = new int[1];
	}
	
	public BigInt(int size) {
		this.size = size;
		digits = new int[this.size];
	}
	
	public BigInt(int size, int[] data) {
		this.size = size;
		this.digits = new int[size];
		for (int i = 0; i < size; i++)
			this.digits[i] = data[i];
	}
	
	public BigInt(BigInt o) {
		this(o.bitCount(), o.getDigits());
	}
	
	public BigInt(String number) {
		number = correctStr(number);
		this.size = number.length();
		int i = this.size - 1;
		this.digits = new int[this.size];
		for (char c : number.toCharArray()) {
			this.digits[i--] = c - '0';
		}
	}
	
	/**
	 * Some static BigInt which are commonly used.
	 */
	static BigInt ZERO = new BigInt();
	static BigInt ONE = new BigInt("1");
	static BigInt TWO = new BigInt("10");
	
	/**
	 * Helper function for correcting input.
	 */
	public String correctStr(String num) {
		String number = "";
		for (int i = 0; i < num.length(); i++) {
			if (i == 0 && (num.charAt(0) == '-' || num.charAt(0) == '+'))
				this.sign = (num.charAt(0) == '+') ? 1 : 0;
			else
				if (num.charAt(i) >= '0' && num.charAt(i) <= '1')
					number += num.charAt(i);
		}
		return number;
	}
	
	/**
	 * String representation of BigInt.
	 */
	public String toString() {
		if (this.zero()) return "0";
		
		String s = "";
		for (int i = 0; i < this.bitCount(); i++) {
			s += this.digits[i];
		}
		s += (this.sign == 1) ? "" : "-";
		return new StringBuilder(s).reverse().toString();
	}
	
	/**
	 * Operations.
	 */
	public BigInt Add(BigInt n) {
		int p1 = this.sign, p2 = n.sign;
		
		if (p1 + p2 == 2)
			return BasicOperations.Add(this, n);
		if (p1 + p2 == 0) {
			BigInt r = BasicOperations.Add(this, n);
			r.sign = 0;
			return r;
		}
		if (p1 == 0) {
			if (compareNoSign(this, n) == -1)
				return BasicOperations.Sub(n, this);
			BigInt r = BasicOperations.Sub(this, n);
			r.sign = 0;
			return r;
		}
		if (p1 == 1) {
			if (compareNoSign(this, n) == -1) {
				BigInt r = BasicOperations.Sub(n, this);
				r.sign = 0;
				return r;
			}
			return BasicOperations.Sub(this, n);
		}
		
		throw new ArithmeticException("Check signs of operands!");
	}
	
	public BigInt Sub(BigInt n) {
		int p1 = this.sign, p2 = n.sign;

		if (p1 + p2 == 2) {
			if (compareNoSign(this, n) == -1) {
				BigInt r = BasicOperations.Sub(n, this);
				r.sign = 0;
				return r;
			}
			return BasicOperations.Sub(this, n);
		}
		if (p1 + p2 == 0) {
			if (compareNoSign(this, n) == -1)
				return BasicOperations.Sub(n, this);
			BigInt r = BasicOperations.Sub(this, n);
			r.sign = 0;
			return r;
		}
		if (p2 == 0)
			return BasicOperations.Add(this, n);
		if (p1 == 0) {
			BigInt r = BasicOperations.Add(this, n);
			r.sign = 0;
			return r;
		}
		
		throw new ArithmeticException("Check signs of operands!");
	}
	
	public BigInt Mult(BigInt n) {
		BigInt res = BasicOperations.Mult(this, n);
		res.sign = (this.sign + n.sign != 1) ? 1 : 0;
		return res;
	}
	
	public BigInt Div(BigInt n) {
		BigInt res = BasicOperations.Div(this, n)[0];
		res.sign = (this.sign + n.sign != 1) ? 1 : 0;
		return res;
	}
	
	public BigInt Mod(BigInt n) {
		BigInt res = BasicOperations.Div(this, n)[1];
		res.sign = (this.sign + n.sign != 1) ? 1 : 0;
		return res;
	}
	
	public BigInt Fac() {
		return new Factorial(this).Fac();
	}
	
	public static BigInt Max(BigInt a, BigInt b) {
		return (a.compareTo(b) < 1) ? b : a;
	}
	
	public static BigInt Min(BigInt a, BigInt b) {
		return (a.compareTo(b) < 1) ? a : b;
	}
	
	/**
	 * Stein's algorithm for greatest common divisor.
	 */
	public BigInt Gcd(BigInt o) {
		BigInt gcd = Gcd_(o);
		gcd.sign = (this.sign + o.sign) == 2 ? 1 : 0;
		return gcd;
	}
	
	private BigInt Gcd_(BigInt o) {
		if (this.compareTo(o) == 0) return this;
		if (this.zero()) return o;
		if (o.zero()) 	 return this;
		
		if (this.getDigits()[0] == 0) {
			if (o.getDigits()[0] == 1)
				return this.rShift(1).Gcd_(o);
			return (this.rShift(1).Gcd_(o.rShift(1))).lShift(1);
		}
		if (o.getDigits()[0] == 0)
			return this.Gcd_(o.rShift(1));
		if (this.compareTo(o) == 1)
			return (BasicOperations.Sub(this, o).rShift(1)).Gcd_(o);
		return (BasicOperations.Sub(o, this).rShift(1)).Gcd_(this);
	}
	
	/**
	 * Binary operations.
	 */
	public BigInt lShift(int n) {
		BigInt shifted = new BigInt(n + this.bitCount());
		
		for (int i = this.bitCount() - 1; i >= 0; i--) 
			shifted.digits[i + n] = this.digits[i];

		return shifted;
	}
	
	public BigInt lShift(BigInt n) {
		BigInt shifted = this;
		
		for (BigInt i = BigInt.ZERO; i.compareTo(n) == -1; i = i.Add(BigInt.ONE))
			shifted = shifted.lShift(1);
		
		return shifted;
	}
	
	public BigInt rShift(int n) {
		if (n >= this.bitCount())
			return BigInt.ZERO;
		
		BigInt shifted = new BigInt(this.bitCount() - n);
		
		for (int i = this.bitCount() - 1; i >= n; i--) 
			shifted.digits[i - n] = this.digits[i];
		
		return shifted;
	}
	
	public BigInt And(BigInt mask) {
		int size = Math.max(this.bitCount(), mask.bitCount());
		BigInt and = new BigInt(size);
		
		for (int i = 0; i < size; i++) {
			int o1 = (i < this.bitCount()) ? this.digits[i] : 0;
			int o2 = (i < mask.bitCount()) ? mask.digits[i] : 0;
			and.digits[i] = o1 & o2;
		}
		
		return and;
	}
	
	public BigInt Or(BigInt mask) {
		int size = Math.max(this.bitCount(), mask.bitCount());
		BigInt or = new BigInt(size);
		
		for (int i = 0; i < size; i++) {
			int o1 = (i < this.bitCount()) ? this.digits[i] : 0;
			int o2 = (i < mask.bitCount()) ? mask.digits[i] : 0;
			or.digits[i] = o1 | o2;
		}
		
		return or;
	}
	
	public BigInt Neg() {
		return null;
	}
	
	/**
	 * @return True if all digits are zero else False
	 */
	public boolean zero() {
		for (int i : this.digits)
			if (i != 0)
				return false;
		return true;
	}
	
	/**
	 * Getters / setters.
	 */
	public int[] 		getDigits() { return digits; }
	public int 			getSign()	{ return sign; }
	/**
	 * @return number of bits excluding leading zeros
	 */
	public int 			bitCount() 	{ 
		int size = this.size;
		for (int i = this.size - 1; i >= 0; i--) {
			if (this.digits[i] == 1)
				return size;
			size--;
		}
		return size;
	}
	
	/**
	 * @return number of set bits
	 */
	public int numOfSetBits() {
		int set = 0;
		for (int i : this.digits)
			set += i;
		return set;
	}
	
	/**
	 * Members.
	 */
	private int[] 		digits = null;
	private int 		size = 1;
	private int 		sign = 1; // 0 - neg; 1 - poz;

	/**
	 * Implementing comparable for our BigInt.
	 * @return -1, 0 or 1 as 'this' is smaller, equal or greather than 'o'
	 */
	@Override
	public int compareTo(BigInt o) {		
		if (this.sign == 0 && o.sign == 1)
			return -1;
		if (this.sign == 1 && o.sign == 0)
			return 1;
		if (this.zero() && o.zero())
			return 0;
		if (this.bitCount() < o.bitCount())
			return -1;
		if (this.bitCount() > o.bitCount())
			return 1;
		
		for (int i = this.bitCount() - 1; i >= 0; i--) {
			int val = (this.sign == 1) ? -1 : 1;
			if (this.getDigits()[i] < o.getDigits()[i])
				return val;
			if (this.getDigits()[i] > o.getDigits()[i])
				return -val;
		}
		
		return 0;
	}
	
	/**
	 * @return numerical presentation of BigInt
	 */
	public int toInt() {
		if (this.bitCount() > 32)
			throw new ArithmeticException("I am too big for an Int");
		int dec = 0;
		for (int i = this.bitCount() - 1; i >= 0; i--) {
			dec <<= 1;
			dec += this.digits[i];
		}
		return dec;
	}
	
	/**
	 * Compare without taking signs into consideration.
	 */
	private static int compareNoSign(BigInt o1, BigInt o2) {
		if (o1.zero() && o2.zero())
			return 0;
		if (o1.bitCount() < o2.bitCount())
			return -1;
		if (o1.bitCount() > o2.bitCount())
			return 1;
		
		for (int i = o1.bitCount() - 1; i >= 0; i--) {
			if (o1.getDigits()[i] < o2.getDigits()[i])
				return -1;
			if (o1.getDigits()[i] > o2.getDigits()[i])
				return 1;
		}
		
		return 0;
	}
	
	private static class BasicOperations {
		
		public static BigInt Add(BigInt x, BigInt y) {
			BigInt sum = new BigInt(Math.max(x.size, y.bitCount()) + 1);
			int carry = 0;
			
			for (int i = 0; i < sum.size; i++) {
				int o1 = (i < x.bitCount()) ? x.digits[i] : 0;
				int o2 = (i < y.bitCount()) ? y.digits[i] : 0;
				
				// full-adder
				sum.getDigits()[i] = ((o1 ^ o2) ^ carry);
				carry = (o1 & o2) | (carry & (o1 ^ o2));
			}
			
			return sum;
		}
		
		public static BigInt Sub(BigInt x, BigInt y) {
			if (compareNoSign(x, y) == -1) throw new IllegalArgumentException();
			
			BigInt sub = new BigInt(x.size);
			int carry = 0;
			
			for (int i = 0; i < sub.size; i++) {
				int o1 = (i < x.bitCount()) ? x.digits[i] : 0;
				int o2 = (i < y.bitCount()) ? y.digits[i] : 0;
				
				// full-subtractor
				sub.getDigits()[i] = ((o1 ^ o2) ^ carry);
				carry = (~o1 & ~o2 &  carry) |
						(~o1 &  o2 & ~carry) |
						(~o1 &  o2 &  carry) |
						( o1 &  o2 &  carry);
			}
			
			return sub;
		}
		
		public static BigInt Mult(BigInt x, BigInt y) {
			if (x.zero() || y.zero())
				return BigInt.ZERO;
			
			BigInt y_ = (x.getDigits()[0] == 1) ? y : BigInt.ZERO;
			return BasicOperations.Add(Mult(x.rShift(1), y).lShift(1), y_);
		}
		
		public static BigInt[] Div(BigInt x, BigInt y) {
			if (y.zero())
				throw new IllegalArgumentException("Cannot divide by zero!");
			if (x.zero())
				return new BigInt[] { BigInt.ZERO, BigInt.ZERO };

			BigInt[] r = {BigInt.ZERO, x};
			if (compareNoSign(x, y) == -1)
				return r;
			
			r = Div(x.rShift(1), y);
			
			r[0] = r[0].lShift(1);
			r[1] = Add(r[1].lShift(1), new BigInt("" + x.getDigits()[0]));
			
			if (compareNoSign(r[1], y) > -1) {
				r[0] = Add(r[0], BigInt.ONE);
				r[1] = Sub(r[1], y);
			}
			
			return r;
		}
	}
	
	/**
	 * Helper class providing fast factorial calculation for BigInt 
	 * using binary split formula.
	 */
	private class Factorial {
		
		public Factorial(BigInt n) {
			num  	= new BigInt(n);
			intVal 	= num.toInt();
			setBits = num.numOfSetBits();
		}
		
		public BigInt Fac() {
			BigInt inner = BigInt.ONE;
			BigInt outer = BigInt.ONE;
			BigInt one   = BigInt.ONE;
			
			for (int i = num.bitCount(); i >= 0; i--) {
				BigInt x = num.rShift(i + 1).Add(one).Or(one);
				BigInt y = num.rShift(i).Add(one).Or(one);
				
				inner = inner.Mult(partProduct(x, y));
				outer = outer.Mult(inner);
			}
			
			return outer.lShift(intVal - setBits);
		}
		
		private BigInt partProduct(BigInt n, BigInt m) {
			BigInt numFactor = m.Sub(n).rShift(1);
			if (numFactor.compareTo(BigInt.TWO) == 0)
				return n.Mult(n.Add(BigInt.TWO));
			if (numFactor.compareTo(BigInt.ONE) == 1) {
			    BigInt mid = n.Add(numFactor).Or(BigInt.ONE);
			    return partProduct(n, mid).Mult(partProduct(mid, m));
			}
			if (numFactor.compareTo(BigInt.ONE) == 0)
				return n;
			
			return BigInt.ONE;
		}
		
		private BigInt num = new BigInt();
		private int intVal, setBits;
	}

}
