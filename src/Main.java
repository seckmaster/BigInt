import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) throws CollectionException {
		if (args.length < 1)
			System.exit(1);
		if (!args[0].equals("calc"))
			System.exit(2);
		
		Calc calculator = new Calc();
		Scanner in = new Scanner(System.in);
		
		while (in.hasNextLine()) {
            calculator.Calculate(in.nextLine());
            System.out.println();
		}
		
		in.close();
	}
	
}
