import java.util.Iterator;

class Calc {
	
	public Calc() {
		operands = new Stack<>();
	}
	
	public void Calculate(String s) throws CollectionException {
		operands.clear();
		String[] tmp = s.split(" ");
		for (String i : tmp) {
			if (isNumeric(i))
				operands.push(new BigInt(i));
			else {
				BigInt res = applyOperation(i);
				if (res != null) {
					operands.push(res);
				}
			}
		}
	}
	
	private BigInt applyOperation(String o) throws CollectionException {
		switch (o) {
		/**
		 * Math operations.
		 */
		case "+":
			return operands.pop().Add(operands.pop());
		case "-":
			BigInt op = operands.pop();
			return operands.pop().Sub(op);
		case "*":
			return operands.pop().Mult(operands.pop());
		case "/":
			BigInt op1 = operands.pop();
			return operands.pop().Div(op1);
		case "%":
			BigInt op2 = operands.pop();
			return operands.pop().Mod(op2);
		case "gcd":
			return operands.pop().Gcd(operands.pop());
		case "!":
			return operands.pop().Fac();
		case "cmp":
			BigInt tmp = operands.pop();
			return new BigInt("" + operands.pop().compareTo(tmp));
		
		/**
		 * Other operations.
		 */
		case "echo":
			if (operands.isEmpty())
				System.out.println();
			else
				System.out.println(operands.top());
			return null;
		case "stack":
			for (Iterator i = operands.getIterator(); i.hasNext();)
				System.out.print(i.next() + " ");
			System.out.println();
			return null;
		case "pop":
			operands.pop();
			return null;
		case "dup":
			operands.push(operands.top());
			return null;
		case "swap":
			tmp = operands.pop();
			BigInt tmp2 = operands.pop();
			operands.push(tmp);
			operands.push(tmp2);
			return null;
		case "rot":
			BigInt x = operands.pop();
			BigInt y = operands.pop();
			BigInt z = operands.pop();
			operands.push(y);
			operands.push(x);
			operands.push(z);
			return null;
		case "rev":
			Stack<BigInt> rev = new Stack<>();
			while (!operands.isEmpty()) {
				rev.push(operands.pop());
			}
			operands = new Stack<>(rev);
			return null;
		case "even":
			BigInt top = operands.pop();
			operands.push(top.getDigits()[0] == 0 ? new BigInt("1") : new BigInt());
			return null;
		case "odd":
			BigInt top1 = operands.pop();
			operands.push(top1.getDigits()[0] == 1 ? new BigInt("1") : new BigInt());
			return null;
		}
		return null;
	}
	
	private static boolean isNumeric(String str) {  
		try {
			Double.parseDouble(str);
		}
		catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}
	
	private Stack<BigInt> operands;
}
